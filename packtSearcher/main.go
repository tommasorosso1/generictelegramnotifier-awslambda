package main

import (
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/aws"
	"fmt"
	"encoding/json"
	"os"
	"golang.org/x/net/html"
	"github.com/PuerkitoBio/goquery"
	"strings"
	"net/http"
)

type Message struct {
	Message   string      `json:"message"`
}

var (
	topicArn = os.Getenv("TOPIC_ARN")
)

func HandleRequest() (string, error) {

	url := "https://www.packtpub.com/packt/offers/free-learning"

	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	htmlNode, err := html.Parse(resp.Body)
	if err != nil {
		return "", err
	}

	title, err := extractTitleFromPage(htmlNode)
	if err != nil {
		return "", err
	}

	fmt.Println(title)

	return publish(title, topicArn)
}

func main() {
	lambda.Start(HandleRequest)

}

func publish(message string, topicArn string) (string, error){

	payload := Message{Message: message}

	svc := sns.New(session.New())

	str, _ := json.Marshal(payload)

	params := &sns.PublishInput{
		Message: aws.String(string(str)),
		TopicArn: aws.String(topicArn),
	}

	resp, err := svc.Publish(params)

	if err != nil {

		fmt.Println(err.Error())
		return "", err
	}

	return fmt.Sprint(resp), nil
}

func extractTitleFromPage(node *html.Node) (string, error) {
	doc := goquery.NewDocumentFromNode(node)

	title := strings.TrimSpace(doc.Find(".dotd-title").First().Find("h2").Text())
	if title != "" {
		return title, nil
	}
	return "", fmt.Errorf("%s", "Title not found, inside html check the selector")
}