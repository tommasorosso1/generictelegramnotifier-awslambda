package main

import (
	"fmt"
	"net/http"
	"io/ioutil"
	"github.com/aws/aws-lambda-go/lambda"
	"os"
	url2 "net/url"
	"github.com/aws/aws-lambda-go/events"
	"encoding/json"
)

type Message struct {
	Message   string      `json:"message"`
}

var (
	apiKey = os.Getenv("TELEGRAM_BOT_API_KEY")
	chatId = os.Getenv("CHAT_ID")
)


func handler(snsEvent events.SNSEvent) {
	for _, record := range snsEvent.Records {
		snsRecord := record.SNS

		message := Message{}
		json.Unmarshal([]byte(snsRecord.Message), &message)

		postOnTelegram(apiKey, chatId, message.Message)

	}
}

func main() {
	lambda.Start(handler)
}

func postOnTelegram(apiKey string, chatId string ,message string) (string, error) {

	escapedMessage := url2.PathEscape(message)

	url := fmt.Sprintf(
		"https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s",
		apiKey,
		chatId,
		escapedMessage)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}


	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}